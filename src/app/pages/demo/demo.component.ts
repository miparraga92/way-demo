import {AfterViewInit, Component, OnInit} from '@angular/core';
import * as moment from 'moment';

declare var vis: any;

@Component({
  selector: 'app-demo',
  templateUrl: './demo.component.html',
  styleUrls: ['./demo.component.scss']
})
export class DemoComponent implements OnInit, AfterViewInit {

  constructor() {
  }

  ngOnInit(): void {
  }

  ngAfterViewInit(): void {
    const element = document.querySelector('#kt_timeline_widget_1_1');
    if (!element) {
      return;
    }

    // Set variables
    const now = Date.now();
    const rootImagePath = element.getAttribute('data-kt-timeline-widget-1-image-root');

    // Build vis-timeline datasets
    const groups = new vis.DataSet([
      {
        id: "research",
        content: "Research",
        order: 1
      },
      {
        id: "qa",
        content: "Phase 2.6 QA",
        order: 2
      },
      {
        id: "ui",
        content: "UI Design",
        order: 3
      },
      {
        id: "dev",
        content: "Development",
        order: 4
      }
    ]);


    const items = new vis.DataSet([
      {
        id: 1,
        group: 'research',
        start: now,
        end: moment(now).add(1.5, 'hours'),
        content: 'Meeting',
        progress: "60%",
        color: 'primary',
        users: [
          'avatars/300-6.jpg',
          'avatars/300-1.jpg'
        ]
      },
      {
        id: 2,
        group: 'qa',
        start: moment(now).add(1, 'hours'),
        end: moment(now).add(2, 'hours'),
        content: 'Testing',
        progress: "47%",
        color: 'success',
        users: [
          'avatars/300-2.jpg'
        ]
      },
      {
        id: 3,
        group: 'ui',
        start: moment(now).add(30, 'minutes'),
        end: moment(now).add(2.5, 'hours'),
        content: 'Landing page',
        progress: "55%",
        color: 'danger',
        users: [
          'avatars/300-5.jpg',
          'avatars/300-20.jpg'
        ]
      },
      {
        id: 4,
        group: 'dev',
        start: moment(now).add(1.5, 'hours'),
        end: moment(now).add(3, 'hours'),
        content: 'Products module',
        progress: "75%",
        color: 'info',
        users: [
          'avatars/300-23.jpg',
          'avatars/300-12.jpg',
          'avatars/300-9.jpg'
        ]
      },
    ]);

    // Set vis-timeline options
    const options = {
      zoomable: false,
      moveable: false,
      selectable: false,
      // More options https://visjs.github.io/vis-timeline/docs/timeline/#Configuration_Options
      margin: {
        item: {
          horizontal: 10,
          vertical: 35
        }
      },

      // Remove current time line --- more info: https://visjs.github.io/vis-timeline/docs/timeline/#Configuration_Options
      showCurrentTime: false,

      // Whitelist specified tags and attributes from template --- more info: https://visjs.github.io/vis-timeline/docs/timeline/#Configuration_Options
      xss: {
        disabled: false,
        filterOptions: {
          whiteList: {
            div: ['class', 'style'],
            img: ['src', 'alt'],
            a: ['href', 'class']
          },
        },
      },
      // specify a template for the items
      template: (item: any) => {
        // Build users group
        const users = item.users;
        let userTemplate = '';
        users.forEach((user: string) => {
          const url: string = rootImagePath + user;
          console.log('users', url)
          userTemplate += '<div class="symbol symbol-circle symbol-25px"><img src="'+url+'" alt="" /></div>';
          console.log('userTemplate', userTemplate)
        });

        return `<div class="rounded-pill bg-light-${item.color} d-flex align-items-center position-relative h-40px w-100 p-2 overflow-hidden">
                    <div class="position-absolute rounded-pill d-block bg-${item.color} start-0 top-0 h-100 z-index-1" style="width: ${item.progress};"></div>

                    <div class="d-flex align-items-center position-relative z-index-2">
                        <div class="symbol-group symbol-hover flex-nowrap me-3">
                            ${userTemplate}
                        </div>

                        <a href="#" class="fw-bold text-white text-hover-dark">${item.content}</a>
                    </div>

                    <div class="d-flex flex-center bg-body rounded-pill fs-7 fw-bolder ms-auto h-100 px-3 position-relative z-index-2">
                        ${item.progress}
                    </div>
                </div>
                `;
      },
    };

    // Init vis-timeline
    const timeline = new vis.Timeline(element, items, groups, options);

    // Prevent infinite loop draws
    timeline.on("currentTimeTick", () => {
      // After fired the first time we un-subscribed
      timeline.off("currentTimeTick");
    });
  }

}
