import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DemoRoutingModule } from './demo-routing.module';
import { DemoComponent } from './demo.component';
import {MatCardModule} from "@angular/material/card";
import {CardsModule} from "../../_metronic/partials";


@NgModule({
  declarations: [
    DemoComponent
  ],
  imports: [
    CommonModule,
    DemoRoutingModule,
    MatCardModule,
    CardsModule
  ]
})
export class DemoModule { }
